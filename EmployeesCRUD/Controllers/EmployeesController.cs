﻿using EmployeesCRUD.Data;
using EmployeesCRUD.Models;
using EmployeesCRUD.Models.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EmployeesCRUD.Controllers
{
    public class EmployeesController : Controller
    {
        private readonly EmployeesDbContext _employeesDbContext;

        public EmployeesController(EmployeesDbContext employeesDbContext)
        {
            _employeesDbContext = employeesDbContext;
        }
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var employees = await _employeesDbContext.Employees.ToListAsync();
            return View(employees);
        }
        public int MyProperty { get; set; }
        [HttpGet]
        public IActionResult Add()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Add(AddEmployeeViewModel addEmployeeViewModel) 
        {
            var employee = new Employee()
            {
                Id = Guid.NewGuid(),
                Name = addEmployeeViewModel.Name,
                Email = addEmployeeViewModel.Email,
                Salary = addEmployeeViewModel.Salary,
                DateOfBirth = addEmployeeViewModel.DateOfBirth,
                Department = addEmployeeViewModel.Department
            };
            await _employeesDbContext.Employees.AddAsync(employee);
            await _employeesDbContext.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        [HttpGet]
        public async Task<IActionResult> View(Guid id)
        {
            var employee = await _employeesDbContext.Employees.FirstOrDefaultAsync(x => x.Id == id);
            if (employee != null)
            {
                var viewModel = new UpdateEmployeeViewModel()
                {
                    Id = employee.Id,
                    Name = employee.Name,
                    Email = employee.Email,
                    Salary = employee.Salary,
                    DateOfBirth = employee.DateOfBirth,
                    Department = employee.Department,
                };
                return await Task.Run(() => View("View", viewModel));
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        public async Task<IActionResult> View(UpdateEmployeeViewModel model)
        {
            var employee = await _employeesDbContext.Employees.FindAsync(model.Id);
            if (employee != null)
            {
                employee.Name = model.Name;
                employee.Email = model.Email;
                employee.Salary = model.Salary;
                employee.DateOfBirth = model.DateOfBirth;
                employee.Department = model.Department;
                await _employeesDbContext.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
        [HttpPost]
        public async Task<IActionResult> Delete(UpdateEmployeeViewModel viewModel)
        {
            var employee = await _employeesDbContext.Employees.FindAsync(viewModel.Id);
            if(employee != null)
            {
                _employeesDbContext.Employees.Remove(employee);
                await _employeesDbContext.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}
