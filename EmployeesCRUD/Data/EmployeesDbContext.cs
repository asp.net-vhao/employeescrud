﻿using EmployeesCRUD.Models.Domain;
using Microsoft.EntityFrameworkCore;

namespace EmployeesCRUD.Data
{
    public class EmployeesDbContext : DbContext
    {
        public EmployeesDbContext(DbContextOptions options) : base(options)
        {
        }
        public DbSet<Employee> Employees { get; set; }
    }
}
